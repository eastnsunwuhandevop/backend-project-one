var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
//company
var signBusinessInformationRouter = require('./routes/regist/company/agency/step1');
var signBusinessCreditCardOrPayPalRouter = require('./routes/regist/company/agency/step2');
var signAddressRouter = require('./routes/regist/company/agency/step3');
var signPersonalRouter = require('./routes/regist/company/agency/step4');
var signAgenceAllRouter = require('./routes/regist/company/agency/step5');
//buyer
var signupBuyerRouter =  require('./routes/regist/buyer/register');
//properties
var signNewDetailRrouter = require('./routes/regist/properties/signnewdetail');
var checkSaleBasicRrouter = require('./routes/regist/properties/checkPropertiysale');
var addSaleRrouter = require('./routes/regist/properties/addnewsale');
var SearchSalebasicRouter =  require('./routes/regist/properties/searchpropertiessalebasic');
var delSaleRouter = require('./routes/regist/properties/delPropertysale');
var editSaleRouter = require('./routes/regist/properties/editPropertysale');

var showUserRouter = require('./routes/showUser');
var deleteUserRouter = require('./routes/deleteUser');
var loginRouter = require('./routes/login');

var app = express();
//设置跨域访问  
app.all('*', function(req, res, next) {  
  res.header("Access-Control-Allow-Origin", "*");  
  res.header("Access-Control-Allow-Headers", "Content-Type, X-Requested-With");  
  res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  if (req.method === "OPTIONS") 
        res.send(200);
    else 
        next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
signBusinessInformationRouter(app);
signBusinessCreditCardOrPayPalRouter(app);
signAddressRouter(app);
signPersonalRouter(app);
signAgenceAllRouter(app);

signupBuyerRouter(app);
//properties
signNewDetailRrouter(app);
checkSaleBasicRrouter(app);
addSaleRrouter(app);
SearchSalebasicRouter(app);
delSaleRouter(app);
editSaleRouter(app);

showUserRouter(app);
deleteUserRouter(app);
loginRouter(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
