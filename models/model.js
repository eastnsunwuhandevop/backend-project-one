const mongoose = require('mongoose')
const DB_URL = 'mongodb://localhost:27017/eastnsun'
mongoose.connect(DB_URL, function(err, db) {
    if (err) {
        console.log('Unable to connect to the Mongodb. Please start the server. Error:', err);
    } else {
        console.log('Connected to Mongodb successfully!');
    }
});

const models = {
	user:{
		'user':{type:String, 'require':true},
		'pwd':{type:String, 'require':true},
		'type':{'type':String, 'require':true},
		//头像
		'avatar':{'type':String},
		// 个人简介或者职位简介
		'desc':{'type':String},
		// 职位名
		'title':{'type':String},
		// 如果你是boss 还有两个字段
		'company':{'type':String},
		'money':{'type':String}
	},
	account:{
        'uid':{type:String, 'require':true},
        'openid':{type:String, 'require':true},
        'login_token':{type:String, 'require':true},
        'login_type':{type:String, 'require':true},
        'type':{type:String, 'require':true},
        'active':{type:Number, 'require':true},
        'token':{type:String, 'require':true}
    },
    creaditcard:{
        'uid':{type:String, 'require':true},
        'card_number':{type:String, 'require':true},
        'card_expiry':{type:String, 'require':true},
    },
    paypalaccount:{
        'uid':{type:String, 'require':true},
        'account_number':{type:String, 'require':true},
        'paypal_expiry':{type:String, 'require':true},
    },
    company:{
        'uid':{type:String, 'require':true},
        'company_uid':{type:String, 'require':true},
        'company_name':{type:String, 'require':true},
        'company_len':{type:String, 'require':true},
        'company_abn':{type:String, 'require':true},
        'company_acn':{type:String, 'require':true},
        'company_phone':{type:String, 'require':true},
        'company_fax':{type:String, 'require':true},
        'company_email':{type:String, 'require':true},
        'company_addr':{type:String, 'require':true},
        'company_addr_post':{type:String, 'require':true},
        'company_addr_fam':{type:String, 'require':true},
        'company_payment_type':{type:String, 'require':true},
    },
    personal:{
        'uid':{type:String, 'require':true},
        'firstname':{type:String, 'require':true},
        'lastname':{type:String, 'require':true},
        'postcode':{type:String, 'require':true},
        'phone':{type:String, 'require':true},
        'dob':{type:String, 'require':true},
        'email':{type:String, 'require':true},
        'licsence_no':{type:String, 'require':true},
        'certificate':{type:String, 'require':true},
    },

    propertysale:{
        'uid':{type:String, 'require':true},
        'proid':{type:String, 'require':true},
        'unit':{type:String, 'require':true},
        'streename':{type:String, 'require':true},
        'streenumber':{type:String, 'require':true},
        'property_suburb':{type:String, 'require':true},
        'postcode':{type:String, 'require':true},
        'state':{type:String, 'require':true},
        'landvolume':{type:String, 'require':true},
        'folio':{type:String, 'require':true},
        'lot':{type:String, 'require':true},
        'plan':{type:String, 'require':true},
        'price':{type:String, 'require':true},
        'duedate':{type:String, 'require':true},
        'propertytype':{type:String, 'require':true},
        'imgstr':{type:String, 'require':true},
    },
    propertysalebasic:{
        'uid':{type:String, 'require':true},
        'proid':{type:String, 'require':true},
        'offered_forsal':{type:String, 'require':true},
        'indicative_sellingprice':{type:String, 'require':true},
    },
    mediansaleprice:{
        'uid':{type:String, 'require':true},
        'proid':{type:String, 'require':true},
        'median_saleprice':{type:String, 'require':true},
        'housenumber':{type:String, 'require':true},
        'periad':{type:String, 'require':true},
        'median_subburb':{type:String, 'require':true},
        'source':{type:String, 'require':true},
    },
    inviteuser:{
        'uid':{type:String, 'require':true},
        'proid':{type:String, 'require':true},
        'name':{type:String, 'require':true},
        'email':{type:String, 'require':true},
        'phone':{type:String, 'require':true},
        'notes':{type:String, 'require':true},
    },
    assign:{
        'uid':{type:String, 'require':true},
        'proid':{type:String, 'require':true},
        'username':{type:String, 'require':true},
        'massage':{type:String, 'require':true},
    },
   
}

for(let m in models){
	mongoose.model(m, new mongoose.Schema(models[m]))
}

module.exports = {
	getModel:function(name){
		return mongoose.model(name)
	}
}