var express = require('express');
var router = express.Router();

var models = require('../../../models/model');
var Propertysale = models.getModel('propertysale');
var Propertysalebasic = models.getModel('propertysalebasic');

module.exports = function (app) {
    app.post("/updatePropertysale", function (req, res) {
        if (req.route.methods.post) {
            if (req.body.unit) {
                const unit = req.body.unit;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type unit'
                })
            }
            if (req.body.streename) {
                const streename = req.body.streename;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type streename'
                })
            }
            if (req.body.streenumber) {
                const streenumber = req.body.streenumber;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type streenumber'
                })
            }
            if (req.body.property_suburb) {
                const property_suburb = req.body.property_suburb;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type property_suburb'
                })
            }
            if (req.body.postcode) {
                const postcode = req.body.postcode;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type postcode'
                })
            }
            if (req.body.state) {
                const state = req.body.state;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type state'
                })
            }
            if (req.body.landvolume) {
                const landvolume = req.body.landvolume;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type landvolume'
                })
            }
            if (req.body.folio) {
                const folio = req.body.folio;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type folio'
                })
            }
            if (req.body.lot) {
                const lot = req.body.lot;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type lot'
                })
            }
            if (req.body.plan) {
                const plan = req.body.plan;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type plan'
                })
            }
            if (req.body.price) {
                const price = req.body.price;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type price'
                })
            }
            if (req.body.duedate) {
                const duedate = req.body.duedate;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type duedate'
                })
            }
            if (req.body.propertytype) {
                const propertytype = req.body.propertytype;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type propertytype'
                })
            }
            if (req.body.imgstr) {
                const imgstr = req.body.imgstr;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type imgstr'
                })
            }
            updatePropertySale(req, res);
        } else {
            return res.json({
                code: 1,
                msg: '404 error'
            })
        }
    })
}

updatePropertySale = (req, res) => {
    const data = {
        'uid': req.body.uid,
        'proid': req.body.proid,
        'unit': req.body.unit,
        'streename': req.body.streename,
        'streenumber': req.body.streenumber,
        'property_suburb': req.body.property_suburb,
        'postcode': req.body.postcode,
        'state': req.body.state,
        'landvolume': req.body.landvolume,
        'folio': req.body.folio,
        'lot': req.body.lot,
        'plan': req.body.plan,
        'price': req.body.price,
        'duedate': req.body.duedate,
        'propertytype': req.body.propertytype,
        'imgstr': req.body.imgstr,
    }
    var conditions = {proid: req.body.proid};  
    var updates = {$set: data};
    Propertysale.update(conditions, updates, function (error) {
        if (error) {  
            return res.json({
                code: 1,
                msg: 'error'
            }) 
        } else {  
            savePropertysalebasic(req, res);
        }  
    })
}

savePropertysalebasic = (req, res) => {
    var offered_forsal = "Unit "+req.body.unit+"/"+req.body.streenumber+" "+req.body.streename+" Ave,"+req.body.property_suburb+" "+req.body.state+" "+req.body.postcode;
    const data = {
        'uid': req.body.uid,
        'proid':  req.body.proid,
        'offered_forsal': offered_forsal,
        'indicative_sellingprice': req.body.indicative_sellingprice,
    }
    var conditions = {proid: req.body.proid};  
    var updates = {$set: data};
    Propertysalebasic.update(conditions, updates, function (error) {
        if (error) {  
            return res.json({
                code: 1,
                msg: 'error'
            })   
        } else {  
            return res.json({
                code: 0,
                msg: 'update ok'
            })
        }  
    })
}