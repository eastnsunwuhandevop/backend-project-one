var express = require('express');
var router = express.Router();

var models = require('../../../models/model');
var Propertysale = models.getModel('propertysale');

module.exports = function (app) {
    app.post("/signNewDetail", function (req, res) {
        if (req.route.methods.post) {
            if (req.body.unit) {
                const unit = req.body.unit;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type unit'
                })
            }
            if (req.body.streename) {
                const streename = req.body.streename;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type streename'
                })
            }
            if (req.body.streenumber) {
                const streenumber = req.body.streenumber;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type streenumber'
                })
            }
            if (req.body.property_suburb) {
                const property_suburb = req.body.property_suburb;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type property_suburb'
                })
            }
            if (req.body.postcode) {
                const postcode = req.body.postcode;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type postcode'
                })
            }
            if (req.body.state) {
                const state = req.body.state;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type state'
                })
            }
        } else {
            return res.json({
                code: 1,
                msg: '404 error'
            })
        }
        
    })
}