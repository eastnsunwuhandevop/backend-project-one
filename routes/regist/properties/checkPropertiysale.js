var express = require('express');
var router = express.Router();

var models = require('../../../models/model');
var Propertysalebasic = models.getModel('propertysalebasic');

module.exports = function (app) {
    app.post("/checkPropertysalebasic", function (req, res) {
        Propertysalebasic.find({}, function (err, docs) {
            if (docs) {
                return res.json({
                    code: 0,
                    msg: 'has propertysalebasic',
                })
            } else {
                return res.json({
                    code: 1,
                    msg: 'no propertysalebasic',
                })
            }
        })
    })
}