var express = require('express');
var router = express.Router();

var models = require('../../../models/model');
var Propertysalebasic = models.getModel('propertysalebasic');
var Propertysale = models.getModel('propertysale');

module.exports = function (app) {
    app.post("/searchSalebasic", function (req, res) {
        if (req.route.methods.post) {
            var searchstr = new RegExp(req.body.search);
            Propertysalebasic.find({offered_forsal:searchstr}, function (err, docs) {
                    return res.json({
                        code: 0,
                        docs
                    })
            })
        } else {
            return res.json({
                code: 1,
                msg: '404 error'
            })
        }
    })
    //点击修改
    app.post("/searchSaleByProid", function (req, res) {
        if (req.route.methods.post) {
            var proid = req.body.proid;
            Propertysale.findOne({proid:proid}, function (err, doc) {
                    return res.json({
                        code: 0,
                        doc
                    })
            })
        } else {
            return res.json({
                code: 1,
                msg: '404 error'
            })
        }
    })
}