var express = require('express');
var router = express.Router();

var models = require('../../../models/model');
var Propertysale = models.getModel('propertysale');
var Propertysalebasic = models.getModel('propertysalebasic');
var md5 = require('md5');
var UUID = require('uuid');

module.exports = function (app) {
    app.post("/registerNewSale", function (req, res) {
        if (req.route.methods.post) {
            if (req.body.unit) {
                const unit = req.body.unit;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type unit'
                })
            }
            if (req.body.streename) {
                const streename = req.body.streename;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type streename'
                })
            }
            if (req.body.streenumber) {
                const streenumber = req.body.streenumber;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type streenumber'
                })
            }
            if (req.body.property_suburb) {
                const property_suburb = req.body.property_suburb;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type property_suburb'
                })
            }
            if (req.body.postcode) {
                const postcode = req.body.postcode;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type postcode'
                })
            }
            if (req.body.state) {
                const state = req.body.state;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type state'
                })
            }
            if(req.body.landvolume){
                const landvolume = req.body.landvolume;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type landvolume'
                })
            }
            if(req.body.folio){
                const folio = req.body.folio;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type folio'
                })
            }
            if(req.body.lot){
                const lot = req.body.lot;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type lot'
                })
            }
            if(req.body.plan){
                const plan = req.body.plan;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type plan'
                })
            }
            if(req.body.price){
                const price = req.body.price;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type price'
                })
            }
            if(req.body.duedate){
                const duedate = req.body.duedate;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type duedate'
                })
            }
            if(req.body.propertytype){
                const propertytype = req.body.propertytype;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type propertytype'
                })
            }
            if(req.body.imgstr){
                const imgstr = req.body.imgstr;
            } else {
                return res.json({
                    code: 1,
                    msg: 'Please type imgstr'
                })
            }
            savePropertySale(req, res);
        } else {
            return res.json({
                code: 1,
                msg: '404 error'
            })
        }
    })
}

savePropertySale = (req, res) => {
    var proid = UUID.v4();
    const data = {
        'uid': req.body.uid,
        'proid': proid,
        'unit': req.body.unit,
        'streename': req.body.streename,
        'streenumber': req.body.streenumber,
        'property_suburb': req.body.property_suburb,
        'postcode': req.body.postcode,
        'state': req.body.state,
        'landvolume': req.body.landvolume,
        'folio': req.body.folio,
        'lot': req.body.lot,
        'plan': req.body.plan,
        'price': req.body.price,
        'duedate': req.body.duedate,
        'propertytype': req.body.propertytype,
        'imgstr': req.body.imgstr,
    }
    const DetailModel = new Propertysale(data);
    DetailModel.save();
    savePropertysalebasic(req, res, proid)
}

savePropertysalebasic = (req, res, proid) => {
    var offered_forsal = "Unit "+req.body.unit+"/"+req.body.streenumber+" "+req.body.streename+" Ave,"+req.body.property_suburb+" "+req.body.state+" "+req.body.postcode;
    const data = {
        'uid': req.body.uid,
        'proid': proid,
        'offered_forsal': offered_forsal,
        'indicative_sellingprice': req.body.indicative_sellingprice,
    }
    const SaleBasicModel = new Propertysalebasic(data);
    SaleBasicModel.save();
    return res.json({
        code: 0,
        msg: 'succ',
        data: req.body
    })
}