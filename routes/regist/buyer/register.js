var express = require('express');
var router = express.Router();
var crypto = require('crypto');

var models = require('../../../models/model');
var Account = models.getModel('account');
var token = {
    createToken: function (obj, timeout) {
        console.log(parseInt(timeout) || 0);
        var obj2 = {
            data: obj,//payload
            created: parseInt(Date.now() / 1000),//token生成的时间的，单位秒
            exp: parseInt(timeout) || 10//token有效期
        };
        //payload信息
        var base64Str = Buffer.from(JSON.stringify(obj2), "utf8").toString("base64");
        //添加签名，防篡改
        var secret = "hel.h-five.com";
        var hash = crypto.createHmac('sha256', secret);
        hash.update(base64Str);
        var signature = hash.digest('base64');
        return base64Str + "." + signature;
    }
}

module.exports = function (app) {
    app.post("/signup", function (req, res) {
        // 判读传输方式
        if (req.route.methods.post) {
            // 判断 是否传输openid
            if (req.body.openid) {
                const openid = req.body.openid;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type email address"
                });
            }
            // 判断 是否传输 password
            if (req.body.login_token) {
                const loginToken = req.body.login_token;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type password"
                });
            }

            validateOpenid(req, res);

        } else {
            return res.json({
                code: 1,
                msg: "404 error",
                data: req.body
            });
        }

    })
}

//account表，通过openid查询用户是否重复
validateOpenid = (req, res) => {
    Account.findOne(
        {
            openid: req.body.openid
        },
        function (err, doc) {
            if (doc) {
                return res.json({
                    code: 1,
                    msg: "Email address is existed"
                });
            } else {
                saveData(req, res);
                return res.json({
                    code: 0,
                    msg: 'succ',
                    data: req.body
                })
            }
        }
    );
}

//注冊
saveData = (req, res) => {
    const password = req.body.login_token;
    console.log(token)
    var miwen = token.createToken(password);
    const data = {
        'uid': req.body.uid,
        'openid': req.body.openid,
        'login_token': miwen,
        'login_type': req.body.login_type,
        'type': req.body.type,
        'active': req.body.active,
    }
    const accountModel = new Account(data);
    accountModel.save();
}