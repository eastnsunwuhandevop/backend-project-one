var express = require('express');
var router = express.Router();

var models = require('../../../../models/model');

module.exports = function (app) {
  app.post('/signaddr', function (req, res) {
    if (req.route.methods.post) {
      if (req.body.company_addr) {
        const company_addr = req.body.company_addr
      } else {
        return res.json({
          code: 1,
          msg: 'Can not find company_addr'
        })
      }

      if (req.body.company_addr_post) {
        const company_addr_post = req.body.company_addr_post
      } else {
        return res.json({
          code: 1,
          msg: 'Can not find company_addr_post'
        })
      }

      if (req.body.company_addr_fam) {
        const company_addr_fam = req.body.company_addr_fam
      } else {
        return res.json({
          code: 1,
          msg: 'Can not find company_addr_fam'
        })
      }

      return res.json({
        code: 0,
        msg: 'succ',
        data: req.body
      })
    } else {
      return res.json({
        code: 1,
        msg: '404 error',
        data: req.body
      })
    }
  })
}