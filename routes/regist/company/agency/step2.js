var express = require('express');
var router = express.Router();

var models = require('../../../../models/model');
var Creaditcard = models.getModel('creaditcard');
var Paypalaccount = models.getModel('paypalaccount');

module.exports = function (app) {
    app.post("/signcre_pay", function (req, res) {
        if (req.route.methods.post) { // 判读传输方式
            if (req.body.payment_type === "card") {
                if (req.body.card_number) {  // 判断 是否传输card_number
                    const card_number = req.body.card_number
                } else {
                    return res.json({
                        code: 1,
                        msg: 'Can not find card_number'
                    })
                }

                if (req.body.expiry) {
                    const expiry = req.body.expiry
                } else {
                    return res.json({
                        code: 1,
                        msg: 'Can not find card_expiry'
                    })
                }
                validateCard_number(req, res);
            } else if (req.body.payment_type === "paypal") {
                if (req.body.account_number) {  // 判断 是否传输account_number
                    const account_number = req.body.account_number
                    
                } else {
                    return res.json({
                        code: 1,
                        msg: 'Can not find account_number'
                    })
                }
                if (req.body.expiry) {
                    const expiry = req.body.expiry
                } else {
                    return res.json({
                        code: 1,
                        msg: 'Can not find paypal_expiry'
                    })
                }
                validateAccount_number(req, res);
            } else {
                return res.json({
                    code: 0,
                    msg: 'no this payment_type',
                })
            }
            return res.json({
                code: 0,
                msg: 'succ',
                data: req.body
            })
        } else {
            return res.json({
                code: 1,
                msg: '404 error',
                data: req.body
            })
        }
    })
}

//creaditcard表，通过card_number查询信用卡受否重复
validateCard_number = (req, res) => {
    Creaditcard.findOne(
        {
            card_number: req.body.card_number
        },
        function (err, doc) {
            if (doc) {
                return res.json({
                    code: 1,
                    msg: "card_number is existed"
                });
            }
        }
    );
}

//paypalaccount表，通过account_number查询是否重复
validateAccount_number = (req, res) => {
    Paypalaccount.findOne(
        {
            account_number: req.body.account_number
        },
        function (err, doc) {
            if (doc) {
                return res.json({
                    code: 1,
                    msg: "account_number is existed"
                });
            }
        }
    );
}