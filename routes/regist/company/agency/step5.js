var express = require('express');
var router = express.Router();

var models = require('../../../../models/model');
var Creaditcard = models.getModel('creaditcard');
var Paypalaccount = models.getModel('paypalaccount');
var Company = models.getModel('company');
var Personal = models.getModel('personal');
var md5 = require('md5');
var UUID = require('uuid');

// 注册詳細信息
module.exports = function (app) {
    app.post("/register", function (req, res) {
        // 判读传输方式
        if (req.route.methods.post) {
            // 判断 paytype
            if (req.body.payment_type === 'card') {
                // 判断 是否传输 card_number
                if (req.body.card_number) {
                    const card_number = req.body.card_number;
                } else {
                    return res.json({
                        code: 1,
                        msg: "Please type card_number"
                    });
                }
                // 判断 是否传输 card_expiry
                if (req.body.expiry) {
                    const expiry = req.body.expiry;
                } else {
                    return res.json({
                        code: 1,
                        msg: "Please type card_expiry"
                    });
                }
            } else if (req.body.payment_type === 'paypal') {
                // 判断 是否传输 account_number
                if (req.body.account_number) {
                    const account_number = req.body.account_number;
                } else {
                    return res.json({
                        code: 1,
                        msg: "Please type payment_type"
                    });
                }
                 // 判断 是否传输 paypal_expiry
                 if (req.body.expiry) {
                    const expiry = req.body.expiry;
                } else {
                    return res.json({
                        code: 1,
                        msg: "Please type paypal_expiry"
                    });
                }
            } else {
                return res.json({
                    code: 1,
                    msg: "Please choose payment_type"
                });
            }

            // 判断 是否传输 company_name
            if (req.body.company_name) {
                const company_name = req.body.company_name;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_name"
                });
            }
            // 判断 是否传输 company_len
            if (req.body.company_len) {
                const company_len = req.body.company_len;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_len"
                });
            }
            // 判断 是否传输 company_abn
            if (req.body.company_abn) {
                const company_abn = req.body.company_abn;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_abn"
                });
            }
            // 判断 是否传输 company_acn
            if (req.body.company_acn) {
                const company_acn = req.body.company_acn;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_acn"
                });
            }
            // 判断 是否传输 company_phone
            if (req.body.company_phone) {
                const company_phone = req.body.company_phone;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_phone"
                });
            }
            // 判断 是否传输 company_fax
            if (req.body.company_fax) {
                const company_fax = req.body.company_fax;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_fax"
                });
            }
            // 判断 是否传输 company_email
            if (req.body.company_email) {
                const company_email = req.body.company_email;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_email"
                });
            }
            // 判断 是否传输 company_addr
            if (req.body.company_addr) {
                const company_addr = req.body.company_addr;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_addr"
                });
            }
            // 判断 是否传输 company_addr_post
            if (req.body.company_addr_post) {
                const company_addr_post = req.body.company_addr_post;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_addr_post"
                });
            }
            // 判断 是否传输 company_addr_fam
            if (req.body.company_addr_fam) {
                const company_addr_fam = req.body.company_addr_fam;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type company_addr_fam"
                });
            }
            // 判断 是否传输 payment_type
            if (req.body.payment_type) {
                const payment_type = req.body.payment_type;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type payment_type"
                });
            }

            // 判断 是否传输 firstname
            if (req.body.firstname) {
                const firstname = req.body.firstname;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type firstname"
                });
            }
            // 判断 是否传输 lastname
            if (req.body.lastname) {
                const lastname = req.body.lastname;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type lastname"
                });
            }
            // 判断 是否传输 postcode
            if (req.body.postcode) {
                const postcode = req.body.postcode;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type postcode"
                });
            }
            // 判断 是否传输 phone
            if (req.body.phone) {
                const phone = req.body.phone;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type phone"
                });
            }
            // 判断 是否传输 dob
            if (req.body.dob) {
                const dob = req.body.dob;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type dob"
                });
            }
            // 判断 是否传输 email
            if (req.body.email) {
                const email = req.body.email;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type email"
                });
            }
            // 判断 是否传输 licsence_no
            if (req.body.licsence_no) {
                const licsence_no = req.body.licsence_no;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type licsence_no"
                });
            }
            // 判断 是否传输 certificate
            if (req.body.certificate) {
                const certificate = req.body.certificate;
            } else {
                return res.json({
                    code: 1,
                    msg: "Please type certificate"
                });
            }

            var uid = UUID.v4();
            if (req.body.payment_type === 'card') {
                validateAll1(req, res, uid);
            } else if (req.body.payment_type === 'paypal') {
                validateAll2(req, res, uid);
            }

        } else {
            return res.json({
                code: 1,
                msg: "404 error",
                data: req.body
            });
        }
    });
};

validateAll1 = (req, res, uid) => {
    Company.findOne(
        {
            company_email: req.body.company_email
        },
        function (err, doc) {
            if (doc) {
                return res.json({
                    code: 1,
                    msg: "company_email has been registered"
                });
            } else {
                Creaditcard.findOne(
                    {
                        card_number: req.body.card_number
                    },
                    function (err, doc) {
                        if (doc) {
                            return res.json({
                                code: 1,
                                msg: "card_number is existed"
                            });
                        } else {
                            Personal.findOne(
                                {
                                    email: req.body.email
                                },
                                function (err, doc) {
                                    if (doc) {
                                        return res.json({
                                            code: 1,
                                            msg: "email has been registered"
                                        });
                                    } else {
                                        saveCreaditcardData(req, res, uid);
                                        saveCompanyData(req, res, uid);
                                        savePersonalData(req, res, uid);
                                        return res.json({
                                            code: 0,
                                            msg: "ok",
                                            data: req.body
                                        })
                                    }
                                }
                            );
                        }
                    }
                );
            }
        }
    );
}

validateAll2 = (req, res, uid) => {
    Company.findOne(
        {
            company_email: req.body.company_email
        },
        function (err, doc) {
            if (doc) {
                return res.json({
                    code: 1,
                    msg: "company_email has been registered"
                });
            } else {
                Paypalaccount.findOne(
                    {
                        account_number: req.body.account_number
                    },
                    function (err, doc) {
                        if (doc) {
                            return res.json({
                                code: 1,
                                msg: "account_number is existed"
                            });
                        } else {
                            Personal.findOne(
                                {
                                    email: req.body.email
                                },
                                function (err, doc) {
                                    if (doc) {
                                        return res.json({
                                            code: 1,
                                            msg: "email has been registered"
                                        });
                                    } else {
                                        savePaypalaAccount(req, res, uid);
                                        saveCompanyData(req, res, uid);
                                        savePersonalData(req, res, uid);
                                        return res.json({
                                            code: 0,
                                            msg: "ok",
                                            data: req.body
                                        })
                                    }
                                }
                            );
                        }
                    }
                );
            }
        }
    );
}

//注冊creaditcard信息
saveCreaditcardData = (req, res, uid) => {
    const data = {
        'uid': uid,
        'card_number': req.body.card_number,
        'card_expiry': req.body.expiry,
    }
    const creaditcardModel = new Creaditcard(data);
    creaditcardModel.save();
}

//注冊paypalaccount信息
savePaypalaAccount = (req, res, uid) => {
    const data = {
        'uid': uid,
        'account_number': req.body.account_number,
        'paypal_expiry': req.body.expiry,
    }
    const PaypalaccountModel = new Paypalaccount(data);
    PaypalaccountModel.save();
}

//注冊公司信息
saveCompanyData = (req, res, uid) => {
    var companyid = UUID.v4();
    const data = {
        'uid': uid,
        'company_uid': companyid,
        'company_name': req.body.company_name,
        'company_len': req.body.company_len,
        'company_abn': req.body.company_abn,
        'company_acn': req.body.company_acn,
        'company_phone': req.body.company_phone,
        'company_fax': req.body.company_fax,
        'company_email': req.body.company_email,
        'company_addr': req.body.company_addr,
        'company_addr_post': req.body.company_addr_post,
        'company_addr_fam': req.body.company_addr_fam,
        'company_payment_type': req.body.payment_type,
    }
    const companyModel = new Company(data);
    companyModel.save();
}

//注冊個人信息
savePersonalData = (req, res, uid) => {
    const data = {
        'uid': uid,
        'givenname': req.body.givenname,
        'surname': req.body.surname,
        'phone': req.body.phone,
        'dob': req.body.dob,
        'email': req.body.email,
        'licsence_no': req.body.licsence_no,
        'certificate_no': req.body.certificate_no,
    }
    const personalModel = new Personal(data);
    personalModel.save();
}
