var express = require('express');
var router = express.Router();

var models = require('../../../../models/model');
var Company = models.getModel('company');

module.exports = function (app) {
    app.post("/signcom", function (req, res) {
        if (req.route.methods.post) { // 判读传输方式
            if (req.body.company_name) {  // 判断 是否传输company_name
                const company_name = req.body.company_name
            } else {
                return res.json({
                    code: 1,
                    msg: 'Can not find company_name'
                })
            }

            if (req.body.company_len) {
                const company_len = req.body.company_len
            } else {
                return res.json({
                    code: 1,
                    msg: 'Can not find company_len'
                })
            }

            if (req.body.company_abn) {
                const company_abn = req.body.company_abn
            } else {
                return res.json({
                    code: 1,
                    msg: 'Can not find company_abn'
                })
            }

            if (req.body.company_acn) {
                const company_acn = req.body.company_acn
            } else {
                return res.json({
                    code: 1,
                    msg: 'Can not find company_acn'
                })
            }

            if (req.body.company_phone) {
                const company_phone = req.body.company_phone
            } else {
                return res.json({
                    code: 1,
                    msg: 'Can not find company_phone'
                })
            }

            if (req.body.company_fax) {
                const company_fax = req.body.company_fax
            } else {
                return res.json({
                    code: 1,
                    msg: 'Can not find company_fax'
                })
            }

            if (req.body.company_email) {
                const company_email = req.body.company_email
            } else {
                return res.json({
                    code: 1,
                    msg: 'Can not find company_email'
                })
            }

            validateCompany_email(req, res);
            return res.json({
                code: 0,
                msg: 'succ',
                data: req.body
            })
        } else {
            return res.json({
                code: 1,
                msg: '404 error',
                data: req.body
            })
        }
    })
}

validateCompany_email = (req, res) => {
    Company.findOne(
        {
            company_email: req.body.company_email
        },
        function (err, doc) {
            if (doc) {
                return res.json({
                    code: 1,
                    msg: "company_email has been registered"
                });
            }
        }
    );
}