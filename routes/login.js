var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var models = require('../models/model');
var Account = models.getModel('account');
var md5 = require('md5');
var app = express();

var token = {
    //创建token
    createToken:function(obj,timeout){
        console.log(parseInt(timeout)||0);
        var obj2={
            data:obj,//payload
            created:parseInt(Date.now()/1000),//token生成的时间的，单位秒
            exp:parseInt(timeout)||10//token有效期
        };
        //payload信息
        var base64Str=Buffer.from(JSON.stringify(obj2),"utf8").toString("base64");
        //添加签名，防篡改
        var secret="hel.h-five.com";
        var hash=crypto.createHmac('sha256',secret);
            hash.update(base64Str);
        var signature=hash.digest('base64');
        return  base64Str+"."+signature;
    },
    //解析token
    decodeToken:function(token){
        var decArr=token.split(".");
        if(decArr.length<2){
            //token不合法
            return false;
        }
        var payload={};
        //将payload json字符串 解析为对象
        try{
            payload=JSON.parse(Buffer.from(decArr[0],"base64").toString("utf8"));
        }catch(e){
            return false;
        }
        
        //检验签名
        var secret="hel.h-five.com";
        var hash=crypto.createHmac('sha256',secret);
            hash.update(decArr[0]);
        var checkSignature=hash.digest('base64');
        return {
            payload:payload,
            signature:decArr[1],
            checkSignature:checkSignature
        }
    },
    //检查token是否过期
    checkToken:function(token){
        var resDecode=this.decodeToken(token);
        if(!resDecode){
            return false;
        }
        //是否过期
        var expState=(parseInt(Date.now()/1000)-parseInt(resDecode.payload.created))>parseInt(resDecode.payload.exp)?false:true;
        if(resDecode.signature===resDecode.checkSignature&&expState){
            return true;
        }
        return false;
    }
}
module.exports = function(app) {
    //检查时间
    app.post("/checktime", function(req, res) {
        console.log(token.checkToken('eyJjcmVhdGVkIjoxNTI1NzQ2ODE4LCJleHAiOjIwfQ==.9E1kZ3HG31/9zp6qzDEexxzxUYVGKN46z7D72Nu+s70='));
    })
    // 登陆
    app.post("/login", function(req, res) {
        // 判读传输方式
        if (req.route.methods.post) {
        // 判断 是否传输openid
          if (req.body.openid) {
            const openid = req.body.openid;
          } else {
            return res.json({
              code: 1,
              msg: "Please type email address"
            });
          }
          // 判断 是否传输 password
          if (req.body.login_token) {
            const loginToken = req.body.login_token;
          } else {
            return res.json({
              code: 1,
              msg: "Please type password"
            });
          }
          Account.findOne({openid: req.body.openid},
                function(err, user) {
                if (user) {
                    if(req.body.login_token){ 
                        var miwen = token.decodeToken(user.login_token).payload.data;
                        console.log(miwen)
                        if(req.body.login_token === miwen){
                            var tokenInfo = token.createToken(user.uid,60*60*3); //登陆成功保存状态时间3小时
                            Account.update({uid:user.uid},{token:tokenInfo},function(e,d){
                                console.log('UPDATE OK');
                            });
                            return res.json({
                                code: 0,
                                msg: '1',
                                token: tokenInfo
                            });
                        } else {                     
                            return res.json({
                                code: 1,
                                msg: "password error"
                            });
                        }
                    } else {
                        return res.json({
                            code: 1,
                            msg: 'Please type password'
                        })
                    }
                } else {
                    return res.json({
                        code: 1,
                        msg: "username does not exist"
                    });
                }
                }
            );
          
        } else {
          return res.json({
            code: 1,
            msg: "404 error",
            data: req.body
          });
        }
      });
};
