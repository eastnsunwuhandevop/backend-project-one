var express = require('express');
var router = express.Router();

var models = require('../models/model');
    Company = models.getModel('company');
    Personal = models.getModel('personal');
var Account = models.getModel('account');
module.exports = function(app) {
    app.get('/query/user/list', function (req, res) {
     
           Account.find({}, function (err, doc) {
             if(doc){
               res.json({
                 code: 0,
                 msg: 'Query successful',
                 doc
               })
             } else {
               res.json({
                 code: 1,
                 msg: 'Can not get data through database 00f0i0n0d000'
               })
             }
           })
        }
       );
       app.get('/query/company/list', function (req, res) {
     
        Company.find({}, function (err, doc) {
          if(doc){
            res.json({
              code: 0,
              msg: 'Query successful',
              doc
            })
          } else {
            res.json({
              code: 1,
              msg: 'Can not get data through database 00f0i0n0d000'
            })
          }
        })
     }
    );
    app.get('/query/personal/list', function (req, res) {
     
      Personal.find({}, function (err, doc) {
        if(doc){
          res.json({
            code: 0,
            msg: 'Query successful',
            doc
          })
        } else {
          res.json({
            code: 1,
            msg: 'Can not get data through database 00f0i0n0d000'
          })
        }
      })
   }
  );
}

